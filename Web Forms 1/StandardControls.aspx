﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StandardControls.aspx.cs" Inherits="StandardControls" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Label ID="Label1" runat="server" Text="Name:"></asp:Label>
        <asp:TextBox ID="NameTB" runat="server" OnTextChanged="NameTB_TextChanged"></asp:TextBox>
        <br />
        <asp:Label ID="Label3" runat="server" Text="Program:"></asp:Label>
        <asp:DropDownList ID="ProgramDDL" runat="server">
            <asp:ListItem>Please select your program</asp:ListItem>
            <asp:ListItem>Software Support</asp:ListItem>
            <asp:ListItem>Software Development</asp:ListItem>
            <asp:ListItem>Network Support</asp:ListItem>
            <asp:ListItem>Network Security</asp:ListItem>
        </asp:DropDownList>
        <br />
        <asp:Button ID="SubmitBtn" runat="server" OnClick="SubmitBtn_Click" Text="Submit" />
        <br />
        <asp:Label ID="WelcomeLbl" runat="server"></asp:Label>
        <br />
        <asp:Label ID="Label2" runat="server" Text="ViewState count:"></asp:Label>
        <asp:Label ID="VSCountLbl" runat="server"></asp:Label>
        <br />
        <asp:Label ID="SessionLbl" runat="server" Text="Session Count:"></asp:Label>
        <asp:Label ID="SessionCountLbl" runat="server" Text="Label"></asp:Label>
    
    </div>
    </form>
</body>
</html>
