using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MVCCoreFullScaffolding.Controllers;
using MVCCoreFullScaffolding.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MVCCoreFullScafTester
{
    [TestClass]
    public class UnitTest1
    {
        private static StudentContext db;

        [ClassInitialize]
        public static void Initialize(TestContext tc)
        {
            DbContextOptions<StudentContext> options = new DbContextOptionsBuilder<StudentContext>().UseInMemoryDatabase(
                                                                             databaseName: "StudentTestDB").Options;
            db = new StudentContext(options);
            db.POS.Add(new POS { Id = 1, ProgramCode = 558, ProgramName = "SWS" });
            db.POS.Add(new POS { Id = 2, ProgramCode = 559, ProgramName = "SWD" });
            db.Student.Add(new Student { Id = 1, FirstName = "Faith", LastName = "Hope",
                                        Birthdate = DateTime.Now.AddYears(-25), StudentNumber = "123456", POSId = 1 });
            db.Student.Add(new Student {
                Id = 2,
                FirstName = "John",
                LastName = "Hopeful",
                Birthdate = DateTime.Now.AddYears(-35),
                StudentNumber = "222222",
                POSId = 2
            });
            db.SaveChanges();
        }

        [TestMethod]
        public async Task StudentIndexTest()
        {
            // Arrange
            StudentController sc = new StudentController(db);

            //Act
            IActionResult res = await sc.Index();

            //Assert
            Assert.IsInstanceOfType(res, typeof(ViewResult));
            ViewResult vr = res as ViewResult;
            Assert.IsInstanceOfType(vr.Model, typeof(List<Student>));
            List<Student> vrm = vr.Model as List<Student>;
            Assert.AreEqual(vrm.Count, 2);
            Assert.AreEqual(vrm[0].FirstName, "Faith");
            Assert.AreEqual(vrm[1].LastName, "Hopeful");
        }
    }
}
