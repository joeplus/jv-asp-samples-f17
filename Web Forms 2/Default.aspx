﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Label ID="Label1" runat="server" Text="Name:"></asp:Label>
<asp:TextBox ID="NameTB" runat="server"></asp:TextBox>
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="NameTB" CssClass="error" ErrorMessage="Name is required!"></asp:RequiredFieldValidator>
<br />
<asp:Label ID="Label2" runat="server" Text="Phone:"></asp:Label>
<asp:TextBox ID="PhoneTB" runat="server" TextMode="Phone"></asp:TextBox>
<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="PhoneTB" CssClass="error" Display="Dynamic" ErrorMessage="Phone is required!"></asp:RequiredFieldValidator>
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="PhoneTB" CssClass="error" Display="Dynamic" ErrorMessage="Phone format is invalid" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}"></asp:RegularExpressionValidator>
<br />
<asp:Label ID="Label3" runat="server" Text="Color:"></asp:Label>
<asp:TextBox ID="ColorTB" runat="server" TextMode="Color"></asp:TextBox>
<br />
<asp:Label ID="Label4" runat="server" Text="Birthdate:"></asp:Label>
<asp:TextBox ID="BirthdateTB" runat="server" TextMode="Date"></asp:TextBox>
<asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="BirthdateTB" CssClass="error" ErrorMessage="Date must be greater than 2010" Operator="GreaterThan" Type="Date" ValueToCompare="2010-1-1"></asp:CompareValidator>
<br />
<asp:Button ID="SubmitBtn" runat="server" OnClick="SubmitBtn_Click" Text="Submit" />
<asp:Button ID="CancelBtn" runat="server" CausesValidation="False" OnClick="CancelBtn_Click" Text="Cancel" UseSubmitBehavior="False" />
</asp:Content>

