﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MVCCoreFullScaffolding.Models;

namespace MVCCoreFullScaffolding.Models
{
    public class StudentContext : DbContext
    {
        public StudentContext (DbContextOptions<StudentContext> options)
            : base(options)
        {
        }

        public DbSet<MVCCoreFullScaffolding.Models.Student> Student { get; set; }

        public DbSet<MVCCoreFullScaffolding.Models.POS> POS { get; set; }
    }
}
