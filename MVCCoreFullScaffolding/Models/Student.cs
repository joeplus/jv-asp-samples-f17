﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MVCCoreFullScaffolding.Models
{
    public class Student
    {
        [Key]
        public int Id { get; set; }
        [Required, Display(Name = "Student Number")]
        public string StudentNumber { get; set; }
        [Required, Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Required, Display(Name = "Last Name")]
        public string LastName { get; set; }
        [DataType(DataType.Date)]
        public DateTime Birthdate { get; set; }
        [ForeignKey("POS")]
        public int POSId { get; set; }
        public virtual POS POS { get; set; }
    }

    public class POS
    {
        [Key]
        public int Id { get; set; }
        [Required, Display(Name = "Program Code")]
        public int ProgramCode { get; set; }
        [Required, Display(Name = "Program Name")]
        public string ProgramName { get; set; }
        public virtual List<Student> Students { get; set; }
    }
}
