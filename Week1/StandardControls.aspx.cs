﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class StandardControls : System.Web.UI.Page
{
    //public string Name { get; set; }
    public StandardControls() : base()
    {
        // Name = NameTB.Text;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            WelcomeLbl.Text = "Welcome to our site.  Fresh page load!";

            if (ViewState["count"] == null)
                ViewState["count"] = 0;
            if (Session["count"] == null)
                Session["count"] = 0;
        }
        else
        {
            WelcomeLbl.Text = "Welcome BACK to our site.  Postback load!";
        }

        ViewState["count"] = (int)ViewState["count"] + 1;
        Session["count"] = (int)Session["count"] + 1;

        VSCountLbl.Text = ViewState["count"].ToString();
        SessionCountLbl.Text = Session["count"].ToString();
    }

    protected void NameTB_TextChanged(object sender, EventArgs e)
    {

    }

    protected void SubmitBtn_Click(object sender, EventArgs e)
    {

    }
}