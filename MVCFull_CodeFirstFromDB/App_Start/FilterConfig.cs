﻿using System.Web;
using System.Web.Mvc;

namespace MVCFull_CodeFirstFromDB
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
