﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC1.Models
{
    public class NewCar
    {
        public int ID { get; set; }
        public string Make { get; set; }
    }
}
