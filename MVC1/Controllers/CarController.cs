using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MVC1.Models;

namespace MVC1.Controllers
{
    public class CarController : Controller
    {
        static List<Car> myCars = new List<Car> { new Car { ID = 1, Make = "Honda", Model ="Accord", Year = 2010, Email = "accord@honda.com"},
                                                  new Car { ID = 2, Make = "Honda", Model ="Civic", Year = 2011, Email = "civic@honda.com"},
                                                  new Car { ID = 3, Make = "Honda", Model ="CRV", Year = 2012, Email = "crv@honda.com"},
        };

        // GET: Car
        public ActionResult Index()
        {
            return View(myCars);
        }

        // GET: Car/Details/5
        public ActionResult Details(int id)
        {
            return View(myCars[id - 1]);
        }

        // GET: Car/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Car/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Car theCar)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid) { 
                    myCars.Add(theCar);
                    return RedirectToAction("Index");
                }
                return View();
            }
            catch
            {
                return View();
            }
        }

        // GET: Car/Edit/5
        public ActionResult Edit(int id)
        {
            return View(myCars[id - 1]);
        }

        // POST: Car/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Car updatedCar)
        {
            try
            {
                // TODO: Add update logic here
                if (ModelState.IsValid)
                {
                    myCars[id - 1] = updatedCar;
                    return RedirectToAction("Index");
                }
                return View(updatedCar);
            }
            catch
            {
                return View();
            }
        }

        // GET: Car/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Car/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}