using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace MVC1.Controllers
{
    public class GreetingsController : Controller
    {
        public GreetingsController() : base()
        {

        }

        public IActionResult Index()
        {
            return View();
        }

        public string Welcome(string name)
        {
            return $"Welcome {name} to ASP.NET MVC!";
        }

        public IActionResult Timestamp()
        {
            ViewData["VDTS"] = DateTime.Now;
            //ViewData["Name"] = "Kareem";
            ViewBag.VBTS = DateTime.Now;

            return View();
        }

    }
}