﻿using MVCFull_CodeFirstFromScratch.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCFull_CodeFirstFromScratch.Controllers
{
    public class PlayerController : Controller
    {
        // GET: Player
        public ActionResult Index()
        {
            using (PlayersTeamsModel db = new PlayersTeamsModel())
            {
                return View(db.Players.ToList());
            }
        }

        public ActionResult SeedDB()
        {
            using (PlayersTeamsModel db = new PlayersTeamsModel())
            {
                if (db.Teams.Count() == 0)
                {
                    Team team1 = new Team { Name = "Raptors" };
                    Team team2 = new Team { Name = "Leafs" };
                    db.Teams.Add(team1);
                    db.Teams.Add(team2);
                    db.SaveChanges();

                    Player player1 = new Player { Name = "Demar", TeamId = team1.Id };
                    Player player2 = new Player { Name = "Austin", TeamId = team2.Id };
                    db.Players.Add(player1);
                    db.Players.Add(player2);
                    db.SaveChanges();
                }
                return View("Index", db.Players.ToList());
            }
        }
    }
}