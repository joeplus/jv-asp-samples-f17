namespace MVCFull_CodeFirstFromScratch.Models
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    public class PlayersTeamsModel : DbContext
    {
        // Your context has been configured to use a 'PlayersTeamsModel' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'MVCFull_CodeFirstFromScratch.Models.PlayersTeamsModel' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'PlayersTeamsModel' 
        // connection string in the application configuration file.
        public PlayersTeamsModel()
            : base("name=PlayersTeamsModel")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<Player> Players { get; set; }
        public virtual DbSet<Team> Teams { get; set; }
    }

    public class Player
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TeamId { get; set; }
        public Team Team { get; set; }
    }

    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual List<Player> Players { get; set; }
    }
}