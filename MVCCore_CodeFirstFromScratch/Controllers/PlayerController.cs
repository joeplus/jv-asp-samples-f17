﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MVCCore_CodeFirstFromScratch.Models;
using Microsoft.EntityFrameworkCore;

namespace MVCCore_CodeFirstFromScratch.Controllers
{
    public class PlayerController : Controller
    {
        private PlayersTeamsModels db;

        public PlayerController(PlayersTeamsModels ctx)
        {
            db = ctx;
        }
        // GET: Player
        public ActionResult Index()
        {
            return View(db.Players.Include("Team").ToList());
        }

        // GET: Player/Details/5
        public ActionResult Details(int id)
        {
            if (db.Players.Any(p => p.Id == id) == false)
                return NotFound();

            return View(db.Players.Include("Team").First(p=>p.Id == id));
        }

        // GET: Player/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Player/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Player/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Player/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Player/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Player/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}