using Microsoft.VisualStudio.TestTools.UnitTesting;
using MVCCoreTestableApp.Controllers;
using Microsoft.AspNetCore.Mvc;
using MVCCoreTestableApp.Models;
using System.Collections.Generic;

namespace MVCTester
{
    [TestClass]
    public class MVCTester
    {
        [TestMethod]
        public void HelloTester()
        {
            // Arrange
            HelloController helloController = new HelloController();

            // Act
            string result = helloController.Welcome();

            // Assert
            Assert.AreEqual(result, "Welcome to ASP.NET Core MVC");
        }
        [TestMethod]
        public void CarDetailsTester()
        {
            // Arrange
            TestCarRepository myTestRepo = new TestCarRepository();
            CarController carController = new CarController(myTestRepo);

            // Act
            ActionResult result = carController.Details(1);

            // Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            ViewResult vResult = result as ViewResult;
            Assert.AreEqual("Details", vResult.ViewName);
            Assert.IsNotNull(vResult.Model);
            Assert.IsInstanceOfType(vResult.Model, typeof(Car));
            Car carResult = vResult.Model as Car;
            Assert.AreEqual("Civic", carResult.Model, "Car model test failed for hard-coded Accord");
        }
    }
}
