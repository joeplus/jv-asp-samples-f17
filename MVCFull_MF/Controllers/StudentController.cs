﻿using MVCFull_MF.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCFull_MF.Controllers
{
    public class StudentController : Controller
    {
        // GET: Car
        public ActionResult Index()
        {
            try
            {
                using (StudentsCoursesContainer db = new StudentsCoursesContainer())
                {
                    if (db.Students.Count() == 0)
                        return HttpNotFound(); // new HttpNotFoundResult();

                    //throw new Exception("Something crazy happened with the DB here!");
                    return View(db.Students.ToList());
                }
            }
            catch (Exception ex)
            {
                string msg = $"Exception retrieving index of cars due to: {ex.Message}";
                Console.WriteLine(msg);
                Debug.WriteLine(msg);
                Trace.WriteLine(msg);
            }
            return View("Error");
        }

        public ActionResult SeedDB()
        {
            using (StudentsCoursesContainer db = new StudentsCoursesContainer())
            {
                if (db.Students.Count() == 0)
                {
                    Course newCourse = new Course { Code = "CO884", Name = "ASP.NET", Description = "Everyone's favorite course" };
                    db.Courses.Add(newCourse);
                    db.SaveChanges();

                    Student newStudent = new Student { Name = "LeBron", StudentNumber = "23232323" };
                    newStudent.Courses.Add(newCourse);
                    db.Students.Add(newStudent);
                    db.SaveChanges();
                }

                return View("Index", db.Students.ToList());
            }
        }
    }
}