﻿using MVCFull.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCFull.Controllers
{
    public class CarController : Controller
    {
        static List<Car> myCars = new List<Car> { new Car { ID = 1, Make = "Honda", Model ="Accord", Year = 2010, Email = "accord@honda.com"},
                                                  new Car { ID = 2, Make = "Honda", Model ="Civic", Year = 2011, Email = "civic@honda.com"},
                                                  new Car { ID = 3, Make = "Honda", Model ="CRV", Year = 2012, Email = "crv@honda.com"},
        };

        // GET: Car
        public ActionResult Index()
        {
            return View(myCars);
        }

        // GET: Car/Details/5
        public ActionResult Details(int id)
        {
            Car theCar = myCars.Find(car => car.ID == id);
            VMCar vmCar = new VMCar
            {
                ID = theCar.ID,
                Email = theCar.Email,
                Make = theCar.Make,
                Model = theCar.Model,
                Rented = true,
                ReturnTime = DateTime.Now.AddDays(1),
                Year = theCar.Year
            };
            return View(vmCar);
        }

        // GET: Car/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Car/Create
        [HttpPost]
        public ActionResult Create(Car myCar)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    myCar.ID = myCars.Count + 1;
                    myCars.Add(myCar);
                    return RedirectToAction("Index");
                }
                return View();
            }
            catch
            {
                return View();
            }
        }

        // GET: Car/Edit/5
        public ActionResult Edit(int id)
        {
            Car theCar = myCars.Find(car => car.ID == id);
            return View(theCar);
        }

        // POST: Car/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Car updatedCar)
        {
            try
            {
                // TODO: Add update logic here
                if (ModelState.IsValid)
                {
                    int index = myCars.FindIndex(car => car.ID == id);
                    if (index != -1)
                    {
                        myCars[index] = updatedCar;
                        return RedirectToAction("Index");
                    }
                    return View();
                }
                return View();
            }
            catch
            {
                return View();
            }
        }

        // GET: Car/Delete/5
        public ActionResult Delete(int id)
        {
            Car theCar = myCars.Find(car => car.ID == id);
            return View(theCar);
        }

        // POST: Car/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                if (ModelState.IsValid)
                {
                    myCars.RemoveAt(id - 1);
                    return RedirectToAction("Index");
                }
                return View();
            }
            catch
            {
                return View();
            }
        }
    }
}
