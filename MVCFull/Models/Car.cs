﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MVCFull.Models
{
    public class Car
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public string Make { get; set; }

        [Required]
        public string Model { get; set; }

        [Range(1920, 2020)]
        public int Year { get; set; }

        [EmailAddress]
        public string Email { get; set; }
    }

    public class VMCar
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public string Make { get; set; }

        [Required]
        public string Model { get; set; }

        [Range(1920, 2020)]
        public int Year { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public bool Rented { get; set; }

        [DataType(DataType.DateTime, ErrorMessage = "Return date format is invalid")]
        public DateTime ReturnTime { get; set; }
    }
}
