﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVCCoreTestableApp.Models
{
    public interface ICarRepository
    {
        IEnumerable<Car> GetCars();
        Car GetCar(int id);
    }
}
