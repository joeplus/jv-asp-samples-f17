﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVCCoreTestableApp.Models
{
    public class StaticCarRepository : ICarRepository
    {
        private static List<Car> myCars = new List<Car> { new Car { Id = 1, Make ="Honda", Model ="Accord", Year = 2015},
            new Car { Id = 2, Make ="Honda", Model ="Civic", Year = 2016},
            new Car { Id = 3, Make ="Honda", Model ="CRV", Year = 2017},
        };

        public Car GetCar(int id)
        {
            return myCars.Find(car => car.Id == id);
        }

        public IEnumerable<Car> GetCars()
        {
            return myCars;
        }
    }
}
