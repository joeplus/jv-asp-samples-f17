﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace MVCCoreTestableApp.Controllers
{
    public class HelloController : Controller
    {
        public string Welcome()
        {
            return "Welcome to ASP.NET Core MVC";
        }
    }
}