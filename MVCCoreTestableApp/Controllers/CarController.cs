﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MVCCoreTestableApp.Models;
using System.Diagnostics;

namespace MVCCoreTestableApp.Controllers
{
    public class CarController : Controller
    {
        private ICarRepository myCarRepo;

        public CarController(ICarRepository repo) : base()
        {
            myCarRepo = repo;
        }
        // GET: Car
        public ActionResult Index()
        {
            return View(myCarRepo.GetCars());
        }

        // GET: Car/Details/5
        public ActionResult Details(int id)
        {
            Debug.WriteLineIf(id == 1, "Debug Details for id 1");
            Trace.WriteLineIf(id == 1, "Trace Details for id 1");
            //return View(myCars[id - 1]);
            return View("Details", myCarRepo.GetCar(id));
        }

        // GET: Car/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Car/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Car/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Car/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Car/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Car/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}